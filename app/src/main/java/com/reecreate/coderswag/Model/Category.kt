package com.reecreate.coderswag.Model

/**
 * Created by reecreate on 15/09/2017.
 */
class Category(val title: String, val image: String) {
    override fun toString(): String {
        return title
    }
}